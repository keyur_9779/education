package com.appp.education.data.appPrefrence

import android.content.Context
import android.content.SharedPreferences
import com.appp.education.EducationApp
import com.appp.education.utils.Constant
import com.infoblox.bloxone.util.AppLogger

/**
 * Global class used to save application state into shared pref;
 * object shoud be singleton class created from application class
 *
 * @author keyur thumar
 */
class AppPrefConfig private constructor() {
    private lateinit var preference: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private val USERID = "userid"
    private val SERVERPROFILE = "serverProfile"
    private val RAWTOKEN = "rawToken"
    private val USERPROFILE = "userProfile"
    private val AUTHENABLED = "bioEnabled"

    fun clearData(){
        editor.putString(USERID, "")
        editor.putString(RAWTOKEN, "")
        editor.putString(USERPROFILE, "")
        editor.commit()

    }

    var userID: String
        get() {
            return preference.getString(USERID, Constant.EMPTY).toString()
        }
        set(data) {
            editor.putString(USERID, data)
            editor.commit()
        }

    var isAuthEnabled: Boolean
        get() {
            return preference.getBoolean(AUTHENABLED, false)
        }
        set(data) {
            editor.putBoolean(AUTHENABLED, data)
            editor.commit()
        }

    var userProfile: String
        get() {
            return preference.getString(USERPROFILE, Constant.EMPTY).toString()
        }
        set(data) {
            editor.putString(USERPROFILE, data)
            editor.commit()
        }
    var rawToken: String
        get() {
            return preference.getString(RAWTOKEN, Constant.EMPTY).toString()
        }
        set(data) {
            editor.putString(RAWTOKEN, data)
            editor.commit()
        }

    var serverProfile: String
        get() {
            return preference.getString(SERVERPROFILE, Constant.EMPTY).toString()
        }
        set(data) {
            editor.putString(SERVERPROFILE, data)
            editor.commit()
        }


    private fun updatedPref() {
        val MYPREFS = "education_pref"

        preference = EducationApp.appContext.getSharedPreferences(
            MYPREFS,
            Context.MODE_PRIVATE
        )
        editor = preference.edit()

    }

    companion object {
        private var appConfig: AppPrefConfig? = null
        val pref: AppPrefConfig
            get() {
                if (appConfig == null) {
                    appConfig =
                        AppPrefConfig()
                }
                return appConfig!!
            }
    }

    /**
     * AppConfig is used for initialising shared preferences object for entire application.
     * it will store state of application like registration,device tokenId, phone number etc.
     */
    init {
        updatedPref()
        AppLogger.logger.D(Constant.TAG, "app config class initialized")
    }
}
