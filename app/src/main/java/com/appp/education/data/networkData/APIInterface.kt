package com.infoblox.bloxone.data.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface APIInterface {

    @GET("api.php")
    fun loadServerConfig(
        @Query("app_id") id: String
    ): Call<ResponseBody?>?


    @FormUrlEncoded
    @POST("validateToken")
    fun authUser(
        @Field("token") token: String
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("getMeeting")
    fun getMeeting(
        @Field("token") token: String,
        @Field("meeting_id") meeting_id: String
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("getApplicationToken")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("school") school: String
    ): Call<ResponseBody?>?

    @GET("getProfileInfo/{userID}/{user_token}")
    fun fetUser(
        @Path("userID") id: String,
        @Path("user_token") user_token: String
    ): Call<ResponseBody?>?

}
