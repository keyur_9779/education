package com.appp.education.data.networkData

import com.appp.education.utils.Constant
import com.infoblox.bloxone.data.network.APIInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class APIClient(var host: String) {
    private lateinit var retroAPI: APIInterface
    private lateinit var client: OkHttpClient

    val getApi: APIInterface get() = retroAPI

    private fun initClient() {
        val builder = OkHttpClient.Builder()
            .followRedirects(true)
            .followSslRedirects(true)
            .retryOnConnectionFailure(true)
            .readTimeout(Constant.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .connectTimeout(Constant.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(Constant.READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
        /* .addInterceptor(object : Interceptor {
             @Throws(IOException::class)
             override fun intercept(chain: Interceptor.Chain): Response {
                 val request: Request = chain.request()
                 AppLogger.logger.D(Constant.TAG, "endpoint https request initiated${request.toString()}")
                 return chain.proceed(request)
             }
         })*/
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        builder.addInterceptor(interceptor)

        client = builder.build()

        /*if (Build.VERSION.SDK_INT < Constant.twenty) {
        enableClearText(builder).build()
    } else {
        getUnsafeOkHttpClient(builder).build()
        //client = builder.build();
    }*/
        // val HOST = "https://env-5.test.infoblox.com/api/atcmcs/v1/"


        val retrofit = Retrofit.Builder()
            .baseUrl(host)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retroAPI = retrofit.create(APIInterface::class.java)
    }


    /*companion object {
        private var apiClient: APIClient? = null
        val getHttpClient: APIClient
            get() {
                if (apiClient == null) {
                    apiClient =
                        APIClient()
                    // AppLogger.logger.D(Constant.TAG, "api client module initialized")
                }
                return apiClient!!
            }
    }*/

    init {
        initClient()
    }
}
