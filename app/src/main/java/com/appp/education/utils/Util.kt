package com.appp.education.utils

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Base64
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.appp.education.EducationApp
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by sameer on 07-12-2017.
 */
object Util {
    const val SECOND = 1000
    private const val MINUTE = 60 * SECOND
    private const val HOUR = 60 * MINUTE
    private const val DAY = 24 * HOUR
    private const val EOF = -1
    private const val DEFAULT_BUFFER_SIZE = 1024 * 4

    fun getResourceId(pVariableName: String, pResourcename: String, pPackageName: String): Int {
        return try {
            EducationApp.appContext.getResources()
                .getIdentifier(pVariableName, pResourcename, pPackageName)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            -1
        }
    }


    @Throws(IOException::class)
    fun from(context: Context, uri: Uri): File {
        val inputStream = context.contentResolver.openInputStream(uri)
        val fileName = getFileName(context, uri)
        val splitName = splitFileName(fileName)
        var tempFile = File.createTempFile(splitName[0], splitName[1])
        tempFile = rename(tempFile, fileName)
        tempFile.deleteOnExit()
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(tempFile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        if (inputStream != null) {
            copy(inputStream, out)
            inputStream.close()
        }
        out?.close()
        return tempFile
    }

    fun String.decode(): String {
        return Base64.decode(this, Base64.DEFAULT).toString(charset("UTF-8"))
    }

    fun String.encode(): String {
        return Base64.encodeToString(this.toByteArray(charset("UTF-8")), Base64.DEFAULT)
    }

    private fun splitFileName(fileName: String?): Array<String?> {
        var name = fileName
        var extension: String? = ""
        val i = fileName!!.lastIndexOf(".")
        if (i != -1) {
            name = fileName.substring(0, i)
            extension = fileName.substring(i)
        }
        return arrayOf(name, extension)
    }

    private fun getFileName(context: Context, uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                cursor?.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf(File.separator)
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    private fun rename(file: File, newName: String?): File {
        val newFile = File(file.parent, newName)
        if (newFile != file) {
            if (newFile.exists() && newFile.delete()) {
                Log.d("FileUtil", "Delete old $newName file")
            }
            if (file.renameTo(newFile)) {
                Log.d("FileUtil", "Rename file to $newName")
            }
        }
        return newFile
    }

    @Throws(IOException::class)
    private fun copy(input: InputStream, output: OutputStream?): Long {
        var count: Long = 0
        var n: Int
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        while (EOF != input.read(buffer).also { n = it }) {
            output!!.write(buffer, 0, n)
            count += n.toLong()
        }
        return count
    }

    fun fullscreenView(activity: AppCompatActivity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE)
        activity.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    fun showMessage(context: Context?, mesg: String?) {
        Toast.makeText(
            context, mesg,
            Toast.LENGTH_SHORT
        ).show()
    }

    // Get the date using calendar object
    val date: String

    // Convert the date into a
    // string using format() method

        // Return the result
        get() {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            // Get the date using calendar object
            val today = Calendar.getInstance()
                .time

            // Convert the date into a
            // string using format() method

            // Return the result
            return df.format(today)
        }

    fun calculateHours(ms: Long): JSONObject {
        var ms = ms
        val jsonObject = JSONObject()
        try {
            if (ms > DAY) {
                val days = (ms / DAY).toInt()
                jsonObject.put("d", "" + days)
                ms %= DAY.toLong()
            } else {
                jsonObject.put("d", "0")
            }
            if (ms > HOUR) {
                val hourss = (ms / HOUR).toInt()
                jsonObject.put("h", "" + hourss)
                ms %= HOUR.toLong()
            } else {
                jsonObject.put("h", "0")
            }
            if (ms > MINUTE) {
                val mint = (ms / MINUTE).toInt()
                jsonObject.put("m", "" + mint)
                ms %= MINUTE.toLong()
            } else {
                jsonObject.put("m", "0")
            }
            Log.d("Keyur", jsonObject.toString())
            return jsonObject
        } catch (e: JSONException) {
        }
        return jsonObject
    }

    fun getDate(data: String?): Date {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            return format.parse(data)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return Calendar.getInstance().time
    }

    fun getFormatedTime(ms: Long): String {
        var ms = ms
        val text = StringBuffer("")
        val jsonObject = JSONObject()
        try {
            if (ms > DAY) {
                val days = (ms / DAY).toInt()
                jsonObject.put("d", "" + days)
                ms %= DAY.toLong()
            } else {
                jsonObject.put("d", "0")
            }
            if (ms > HOUR) {
                val hourss = (ms / HOUR).toInt()
                jsonObject.put("h", "" + hourss)
                ms %= HOUR.toLong()
            } else {
                jsonObject.put("h", "0")
            }
            if (ms > MINUTE) {
                val mint = (ms / MINUTE).toInt()
                jsonObject.put("m", "" + mint)
                ms %= MINUTE.toLong()
            } else {
                jsonObject.put("m", "0")
            }
            val d = jsonObject.getInt("d")
            val h = jsonObject.getInt("h")
            val m = jsonObject.getInt("m")
            if (d >= 1) {
                if (h == 0 && m == 0) {
                    text.append(d).append(" day.")
                } else {
                    text.append(d).append(" day")
                }
                if (h >= 1) {
                    text.append(", ").append(h).append(" hour")
                }
                if (m >= 1) {
                    text.append(", ").append(m).append(" min.")
                }
                return text.toString()
            }
            if (d == 0) {
                if (h >= 1) {
                    if (m == 0) {
                        text.append(h).append(" hour.")
                    } else {
                        text.append(h).append(" hour")
                    }
                }
                if (m >= 1) {
                    if (h == 0) {
                        text.append(m).append(" min.")
                    } else {
                        text.append(", ").append(m).append(" min.")
                    }
                }
                return text.toString()
            }
            if (h == 0) {
                if (m >= 1) {
                    text.append(m).append(" min.")
                }
            }
            Log.d("Keyur", jsonObject.toString())
        } catch (e: JSONException) {
        }
        return text.toString()
    }

    fun parseDateDOT(starttime: String): String {
        var month = starttime.substring(5, 7)
        val day = starttime.substring(8, 10)
        month = when (month) {
            "01" -> "January"
            "02" -> "February"
            "03" -> "March"
            "04" -> "April"
            "05" -> "May"
            "06" -> "June"
            "07" -> "July"
            "08" -> "August"
            "09" -> "September"
            "10" -> "October"
            "11" -> "November"
            else -> "December"
        }
        return "$day. $month"
    }
}