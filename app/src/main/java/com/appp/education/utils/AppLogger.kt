package com.infoblox.bloxone.util

import android.util.Log
import com.appp.education.utils.Constant

class AppLogger private constructor() {
    private var mPrintLog = true

    fun D(tag: String?, message: String?) {
        if (mPrintLog)
            message?.let {
                Log.d("keyur", it)
            }
    }

    fun E(tag: String?, message: String?) {
        if (mPrintLog) message?.let {
            Log.d("keyur", it)
        }
    }

    fun W(tag: String?, message: String?) {
        if (mPrintLog) message?.let { Log.d("keyur", it) }
    }

    fun I(tag: String?, message: String?) {
        if (mPrintLog) message?.let { Log.d("keyur", it) }
    }

    fun V(tag: String?, message: String?) {
        if (mPrintLog) message?.let { Log.d("keyur", it) }
    }


    companion object {
        private var mLoggerIntance: AppLogger? = null
        val logger: AppLogger
            get() {
                if (mLoggerIntance == null) {
                    mLoggerIntance = AppLogger()
                }

                return mLoggerIntance!!
            }
    }

    init {

        D(
            Constant.TAG,
            "Logger is now initialize"
        )
    }
}
