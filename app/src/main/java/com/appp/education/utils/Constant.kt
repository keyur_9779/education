package com.appp.education.utils

object Constant {
    const val WORK_TIME = 5000


    // TODO Change it to your web domain
    var WEB_DOMAIN = "zoom.us"

    // TODO change it to your user ID
    var USER_ID = "Your user ID from REST API"

    // TODO change it to your token
    var ZOOM_ACCESS_TOKEN = "Your zak from REST API"

    // TODO Change it to your exist meeting ID to start meeting
    var MEETING_ID: String = ""

    /**
     * We recommend that, you can generate jwttoken on your own server instead of hardcore in the code.
     * We hardcore it here, just to run the demo.
     *
     * You can generate a jwttoken on the https://jwt.io/
     * with this payload:
     * {
     * "appKey": "string", // app key
     * "iat": long, // access token issue timestamp
     * "exp": long, // access token expire time
     * "tokenExp": long // token expire time
     * }
     */


    var HOSTURL = "https://mobileapp.eto.school/"
    const val userStatus: String = "active"
    const val CLIENTDATA: String = "ffed"
    const val NO_OPERATION = "NO_OPERATION"
    const val USERID = "userId"
    const val GROUPNAME = "endPointGroupName"
    const val ENDPOINT_URL = "cspUrl"
    const val AUTHTOKEN = "authToken"
    const val DNSENABLED = "allowServiceControl"
    const val DEVICE_PLATFORM: String = "android"
    const val LIKE_QUERY = " LIKE ?"
    const val EMPTY = ""
    const val one = 1
    const val two = 2
    const val three = 3
    const val zero = 0
    const val twenty = 20
    const val fifteen = 15
    const val TAG = "keyur"
    const val NAME = "keyur"
    const val READ_TIMEOUT = 30
    const val DNS_STOP_TIMEOUT = 5
    const val MS = 1000
    const val API_RETRY_REGISTER = 60
    const val API_RETRY_LOGIN = 30
    const val API_RETRY_HEALTH_CHECK = 600
    const val API_TIMER = 600
    const val API_RETRY_HEALTH_CHECK_PENDING = 60
    const val GROUP = "Group3"

    const val REQUEST_CONNECT = 1

    const val LOCK_REQUEST_CODE = 221
    const val SECURITY_SETTING_REQUEST_CODE = 233

    const val ZOOMSDK = "0EDrCsYKcPrfAc21RQcxyK6oVXrRLpVuGwiz"
    const val ZOOM_SECKRE = "41JEeapaQPThYIsI5I9lkKHymIkRwSNO1aCd"

}
