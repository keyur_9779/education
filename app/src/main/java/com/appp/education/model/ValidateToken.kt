package com.appp.education.model

data class ValidateToken(
    val token: String
)