package com.appp.education.model

data class ConfigServerModel(
    val api_path: String,
    val consultation_text: String? = null,
    val consultation_url: String,
    val home_path: String,
    val id: String,
    val last_rollout: String,
    val latest_version: String,
    val logout_url: String,
    val name: String,
    val online_class_url_s: String,
    val online_class_url_t: String,
    var reg_text: String? = null,
    val reg_url: String,
    val school_id: String,
    val show_app_banner_for: String,
    val tiles_c: String,
    val tiles_h: String,
    val tiles_s: String,
    val tiles_t: String
)