package com.appp.education.model

data class UserAuth(
    val username: String,
    val password: String
)