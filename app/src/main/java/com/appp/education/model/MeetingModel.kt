package com.appp.education.model

data class MeetingModel(
    val display_name: String,
    val jwt: String,
    val jwt_1: String,
    val type: String,
    var zak: String?=null,
    val zoom_join_url: String,
    val zoom_meeting_id: String,
    val zoom_password: String,
    val zoom_sdk_key: String,
    val zoom_sdk_secret: String,
    val zoom_start_url: String
)