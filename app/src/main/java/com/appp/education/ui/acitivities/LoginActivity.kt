package com.appp.education.ui.acitivities

import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.appp.education.BuildConfig
import com.appp.education.EducationApp
import com.appp.education.data.appPrefrence.AppPrefConfig
import com.appp.education.data.networkData.APIClient
import com.appp.education.databinding.ActivityLoginBinding
import com.appp.education.utils.Constant
import com.appp.education.utils.Util
import com.infoblox.bloxone.util.AppLogger
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.nio.charset.StandardCharsets

class LoginActivity : AppCompatActivity() {

    private lateinit var activityLoginData: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityLoginData = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(activityLoginData.root)

        titleTV.typeface = EducationApp.mediumFont
        registrationButton.typeface = EducationApp.mediumFont
        consultationButton.typeface = EducationApp.mediumFont
        passwordET.typeface = EducationApp.mediumFont
        userET.typeface = EducationApp.mediumFont
        userName.typeface = EducationApp.mediumFont
        password.typeface = EducationApp.mediumFont
        hintTV.typeface = EducationApp.mediumFont
        versionTV.typeface = EducationApp.mediumFont
        loginButton.typeface = EducationApp.mediumFont


        if (BuildConfig.DEBUG) {
            userET.setText("6856453")
            passwordET.setText("demo4567")
        }

        EducationApp.configModel.reg_text?.let {
            if (it.isNotEmpty())
                activityLoginData.registrationButton.apply {
                    visibility = View.VISIBLE
                    text = it
                    setOnClickListener {
                        startActivity(
                            Intent(this@LoginActivity, WebViewActivity::class.java)
                                .putExtra(
                                    Constant.CLIENTDATA,
                                    "${EducationApp.configModel.home_path}/${EducationApp.configModel.reg_url}"
                                )
                                .putExtra(Constant.AUTHTOKEN, Constant.EMPTY)
                                .putExtra(Constant.NAME, text as String)
                        )
                    }
                }
        }

        EducationApp.configModel.consultation_text?.let {
            if (it.isNotEmpty())
                activityLoginData.consultationButton.apply {
                    visibility = View.VISIBLE
                    text = it
                    setOnClickListener {

                        startActivity(
                            Intent(this@LoginActivity, WebViewActivity::class.java)
                                .putExtra(
                                    Constant.CLIENTDATA,
                                    "${EducationApp.configModel.home_path}/${EducationApp.configModel.consultation_url}"
                                )
                                .putExtra(Constant.AUTHTOKEN, Constant.EMPTY)
                                .putExtra(Constant.NAME, text as String)
                        )
                    }
                }
        }

        activityLoginData.loginButton.setOnClickListener {

            doLogin()
        }

        activityLoginData.passwordET.setOnKeyListener { v, _, event ->

            if ((event.action == KeyEvent.ACTION_DOWN)) {

                doLogin();
            }
            return@setOnKeyListener true
        }

    }

    private fun doLogin() {
        val userName = activityLoginData.userET.text.toString()
        val password = activityLoginData.passwordET.text.toString()

        if (userName.isEmpty()) {
            activityLoginData.userET.error = "Login ID is empty"
            return@doLogin
        }
        if (password.isEmpty()) {
            activityLoginData.passwordET.error = "Password is empty"
            return@doLogin
        }


        val call =
            APIClient("${EducationApp.configModel.api_path}/").getApi.login(

                userName,
                password,
                EducationApp.configModel.school_id

            )
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.body() != null) {
                    try {
                        val data = response.body()!!.string()
                        AppLogger.logger.D("Keyur", "login data $data")

                        AppPrefConfig.pref.rawToken = data;
                        val byteArray: ByteArray = Base64.decode(data, Base64.DEFAULT)
                        val decodedData = String(byteArray, StandardCharsets.UTF_8)
                        AppLogger.logger.D("Keyur", decodedData)

                        AppPrefConfig.pref.userID = decodedData

                        if (data.endsWith("=")) {
                            initView(500, true)
                        } else {
                            Util.showMessage(
                                applicationContext,
                                "$data invalid username or password"
                            )
                            initView(1000)
                        }
                    } catch (e: Exception) {
                        initView(1000)
                        AppLogger.logger.D("Keyur", "failed data ${e.message}")

                    }
                } else {
                    val eBody = response.errorBody()
                    eBody?.let { Util.showMessage(applicationContext, it.string()) }

                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                // materialDialog.dismiss();
                Util.showMessage(applicationContext, t.message)
                AppLogger.logger.D("Keyur", "failed data ${t.message}")
            }
        })
    }

    private fun initView(i: Int, b: Boolean = false) {

        startActivity(Intent(this, MainActivity::class.java))
        finish()

    }
}