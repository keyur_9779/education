package com.appp.education.ui.acitivities

import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.appp.education.EducationApp
import com.appp.education.data.appPrefrence.AppPrefConfig
import com.appp.education.data.networkData.APIClient
import com.appp.education.databinding.WebviewActivityBinding
import com.appp.education.model.MeetingModel
import com.appp.education.utils.Constant
import com.appp.education.utils.Util
import com.appp.education.utils.ZoomMeetingUISettingHelper
import com.google.gson.Gson
import com.infoblox.bloxone.util.AppLogger
import kotlinx.android.synthetic.main.webview_activity.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import us.zoom.sdk.*
import java.util.*

class WebViewActivity : AppCompatActivity() {
    lateinit var binding: WebviewActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = WebviewActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)

        header_title.typeface = EducationApp.mediumFont


        val webSettings = webViewData.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.allowFileAccess = true
        webSettings.userAgentString = "AndroidApp"
        webViewData.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

                AppLogger.logger.D(Constant.TAG, url);


                if (url.contains("zClient?") /*|| url.contains("conference-meeting/start") || url.contains(
                        "conference-meeting/join"
                    )*/
                ) {
// https://demo-v1.eto.training/conference-meeting/join/4416


                    val moreDetailed = if (url.contains("zClient?")) {
                        val splitUrl = url.split("?")
                        for (it in splitUrl) {
                            AppLogger.logger.D("Keyur", "data url splited ${it}")

                        }

                        val meetingURL = splitUrl[1].split("&")

                        /*val moreDetailed = */meetingURL[0].split("=")[1]


                    } else {
                        val meetingURL = url.split("/")

                        meetingURL[meetingURL.size - 1]
                    }


                    val call =
                        APIClient("${EducationApp.configModel.api_path}/").getApi.getMeeting(
                            AppPrefConfig.pref.rawToken, moreDetailed
                        )
                    call!!.enqueue(object : Callback<ResponseBody?> {
                        override fun onResponse(
                            call: Call<ResponseBody?>,
                            response: Response<ResponseBody?>
                        ) {
                            if (response.body() != null) {
                                try {
                                    val data = response.body()!!.string()
                                    AppLogger.logger.D("Keyur", data)

                                    val meetingModel =
                                        Gson().fromJson(data, MeetingModel::class.java)

                                    meetingModel?.let {

                                        val zoomSDK = ZoomSDK.getInstance()


                                        val initParams = ZoomSDKInitParams()
                                        //initParams.jwtToken = it.jwt_1
                                        initParams.domain = "us02web.zoom.us"
                                        initParams.appSecret =
                                            "41JEeapaQPThYIsI5I9lkKHymIkRwSNO1aCd"
                                        initParams.appKey = "0EDrCsYKcPrfAc21RQcxyK6oVXrRLpVuGwiz"
                                        initParams.enableLog = true

                                        zoomSDK.initialize(this@WebViewActivity, object :
                                            ZoomSDKInitializeListener {
                                            override fun onZoomSDKInitializeResult(
                                                errorCode: Int,
                                                internalErrorCode: Int
                                            ) {


                                                if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {

                                                    Util.showMessage(
                                                        applicationContext,
                                                        "Failed to initialize Zoom SDK. Error: $errorCode, internalErrorCode=$internalErrorCode"
                                                    )

                                                } else {

                                                    Util.showMessage(
                                                        applicationContext,
                                                        "Initialize Zoom SDK successfully"
                                                    )

                                                    registerMeetingServiceListener()
                                                    startMeeting(
                                                        it.zak,
                                                        it.zoom_meeting_id,
                                                        it.zoom_password
                                                    )
                                                }


                                            }

                                            override fun onZoomAuthIdentityExpired() {

                                                Util.showMessage(
                                                    applicationContext,
                                                    "Zoom Auth Identity Expired"
                                                )
                                            }
                                        }, initParams)
                                    }


                                } catch (e: Exception) {

                                    AppLogger.logger.D("Keyur", "failed data ${e.message}")

                                }
                            } else {
                                Util.showMessage(applicationContext, response.errorBody()?.string())


                            }
                        }

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            // materialDialog.dismiss();
                            Util.showMessage(applicationContext, t.message)
                            AppLogger.logger.D("Keyur", "failed data ${t.message}")

                        }
                    })

                } else {
                    AppLogger.logger.D(Constant.TAG, "url is getting loaded throught webview");

                    view.loadUrl(url)
                }
                return true
            }
        }
        webViewData.webChromeClient = object : WebChromeClient() {
            override fun onJsBeforeUnload(
                view: WebView?,
                url: String?,
                message: String?,
                result: JsResult?
            ): Boolean {

                view?.loadUrl(url!!)
                return true
            }
        }
        if (Build.VERSION.SDK_INT >= 19) {
            webViewData.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else if (Build.VERSION.SDK_INT in 11..18) {
            webViewData.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }

        val authToken = intent.getStringExtra(Constant.AUTHTOKEN)
        val url = intent.getStringExtra(Constant.CLIENTDATA)
        val title = intent.getStringExtra(Constant.NAME)
        header_title.text = title
        val cookie = CookieManager.getInstance().getCookie(url)

        if (cookie != null) {
            if (!cookie.contains(authToken!!)) {
                CookieManager.getInstance().setCookie(url, "auth_hash=$authToken;")
                CookieManager.getInstance()
                    .setCookie(url, "selected_school=${EducationApp.configModel.school_id};")
            }
        } else {
            CookieManager.getInstance().setCookie(url, "auth_hash=$authToken;")
            CookieManager.getInstance()
                .setCookie(url, "selected_school=${EducationApp.configModel.school_id};")
        }

        val cookie2 = CookieManager.getInstance().getCookie(url)

        AppLogger.logger.D(Constant.TAG, "cookieManager $cookie2")


        webViewData.loadUrl(url!!)

        menuItem.setOnClickListener { finish() }


    }

    private fun registerMeetingServiceListener() {
        val zoomSDK = ZoomSDK.getInstance()
        val meetingService = zoomSDK.meetingService
        meetingService?.addListener { meetingStatus, errorCode, internalErrorCode ->
            if (meetingStatus == MeetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) {
                Util.showMessage(applicationContext, "Version of ZoomSDK is too low!")
            }

            if (meetingStatus == MeetingStatus.MEETING_STATUS_IDLE || meetingStatus == MeetingStatus.MEETING_STATUS_FAILED) {
                Util.showMessage(applicationContext, "Meeting is IDLE or failed")

            }


        }
    }

    fun startMeeting(ZOOM_ACCESS_TOKEN: String?, MEETING_ID: String, password: String) {

        val dataArray = AppPrefConfig.pref.userID.split("|")


        val userData = AppPrefConfig.pref.userProfile
        val userProfile = userData.split("|")

//        val meetingService = zoomSDK.meetingService
        //      val opts = StartMeetingOptions()

        val dataUser = AppPrefConfig.pref.userID.split("|")


        if (dataUser[0] == "h" || dataUser[0] == "t") {

            startMeetingWithHost(ZOOM_ACCESS_TOKEN!!, MEETING_ID, userProfile[0], dataUser[1])
        } else {
            joinMeeting(MEETING_ID, userProfile[0], dataUser[1], password)
        }
/*

        opts.no_driving_mode = false
        opts.no_meeting_end_message = false
        opts.no_titlebar = false
        opts.no_bottom_toolbar = false
        opts.no_invite = true
        val params = StartMeetingParamsWithoutLogin()
        params.userId = dataArray[1]
        params.zoomAccessToken = ZOOM_ACCESS_TOKEN
        params.meetingNo = MEETING_ID
        params.displayName = userProfile[0]
        val ret = meetingService.startMeetingWithParams(this, params, opts)
        AppLogger.logger.D("keyu", "onClickBtnStartMeeting, ret=$ret")*/
    }

    private fun joinMeeting(
        meetingId: String,
        uName: String,
        userId: String,
        meetingPassword: String
    ) {
        val meetingService: MeetingService = ZoomSDK.getInstance().getMeetingService()
        val opts = ZoomMeetingUISettingHelper.getJoinMeetingOptions()
        val params = JoinMeetingParam4WithoutLogin()

        params.displayName = uName
        params.meetingNo = meetingId
        params.password = meetingPassword
        meetingService.joinMeetingWithParams(this, params, opts)
    }

    private fun startMeetingWithHost(
        zak: String,
        meetingId: String,
        uName: String,
        userId: String
    ) {

        val mZoomSDK = ZoomSDK.getInstance()
        if (!mZoomSDK.isInitialized) {
            Toast.makeText(
                applicationContext,
                "ZoomSDK has not been initialized successfully",
                Toast.LENGTH_LONG
            )
                .show()
            return
        }

        val meetingService: MeetingService = mZoomSDK.meetingService
        val opts: StartMeetingOptions = ZoomMeetingUISettingHelper.getStartMeetingOptions()

        val params = StartMeetingParamsWithoutLogin()
        params.userId = userId
        params.userType = MeetingService.USER_TYPE_API_USER
        params.displayName = uName
        params.meetingNo = meetingId
        params.zoomAccessToken = zak
        val ret = meetingService.startMeetingWithParams(this, params, opts)
        AppLogger.logger.D("keyu", "onClickBtnStartMeeting, ret=$ret")
    }


    override fun onBackPressed() {
        if (!webViewData.canGoBack()) {
            super.onBackPressed()
        } else {
            webViewData.goBack()
        }
    }
}