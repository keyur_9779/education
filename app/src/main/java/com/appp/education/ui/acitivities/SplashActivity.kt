package com.appp.education.ui.acitivities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.appp.education.BuildConfig
import com.appp.education.EducationApp
import com.appp.education.R
import com.appp.education.data.appPrefrence.AppPrefConfig
import com.appp.education.data.networkData.APIClient
import com.appp.education.model.ConfigServerModel
import com.appp.education.utils.Constant.HOSTURL
import com.appp.education.utils.Util
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.infoblox.bloxone.util.AppLogger
import kotlinx.android.synthetic.main.splash_screen.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by THKEYUR on 31/7/2021.
 */
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Util.fullscreenView(this@SplashActivity)
        setContentView(R.layout.splash_screen)
        Glide.with(this).asGif().load(R.raw.loader).into(splash_logo);


        val url =
            "https://demo-v1.eto.training/zClient?meeting=4412&apiKey=zgxepy8AQJiz13Skg5tDpQ&mn=87664868866&name=RGlsbmE=&pwd=2252&leaveUrl=https://demo-v1.eto.training/conference-meetings&role=1&email=ZGlsQGdtYWlsLmNvbQ==&lang=en&signature=emd4ZXB5OEFRSml6MTNTa2c1dERwUS44NzY2NDg2ODg2Ni4xNjI4MDc1OTg0MDAwLjEucE9WbmtTalAvRXdWN25CZ1pDaHhvZ3h2RUNmb1VpZU1aSDBjS09hY3lsOD0"


        /*val splitUrl = url.split("?")
        for (it in splitUrl) {
            AppLogger.logger.D("Keyur", "data url splited ${it}")

        }

        val meetingURL = splitUrl[1].split("&")

        for (it in meetingURL) {
            AppLogger.logger.D("Keyur", "data url splited ${it}")

            val moreDetailed = it.split("=")
            for (ii in moreDetailed) {
                AppLogger.logger.D("Keyur", "data url detailas ${ii}")

            }

        }*/

        /*val splitUrl = url.split("?")
        for (it in splitUrl) {
            AppLogger.logger.D("Keyur", "data url splited ${it}")

        }

        val meetingURL = splitUrl[1].split("&")

        val moreDetailed = meetingURL[0].split("=")

        AppLogger.logger.D("Keyur", "data url dasdasd ${moreDetailed[1]}")
*/

        getServerConfig()

        welcomText.setTypeface(EducationApp.mediumFont)
        versionTV.setTypeface(EducationApp.mediumFont)

        versionTV.text = "Version ${BuildConfig.VERSION_NAME}"
    }

    private fun validateToken() {

        val call =
            APIClient("${EducationApp.configModel.api_path}/").getApi.authUser(
                AppPrefConfig.pref.rawToken
            )
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.body() != null) {
                    try {
                        val data = response.body()!!.string()
                        AppLogger.logger.D("Keyur", data)
                        if (data.equals("valid")) {
                            initView(500, true)
                        } else {
                            initView(1000)
                        }
                    } catch (e: Exception) {
                        initView(1000)
                        AppLogger.logger.D("Keyur", "failed data ${e.message}")

                    }
                } else {
                    initView(1000)
                    AppLogger.logger.D("Keyur", "failed dataasdasdasdasdas")

                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                // materialDialog.dismiss();
                initView(1000)
                AppLogger.logger.D("Keyur", "failed data ${t.message}")

            }
        })
    }

    fun initView(time: Int, isvalid: Boolean = false) {
        Handler(Looper.myLooper()!!).postDelayed({
            if (EducationApp.isConnected) {
                val i = if (isvalid) {
                    Intent(this@SplashActivity, MainActivity::class.java)
                } else {
                    Intent(this@SplashActivity, LoginActivity::class.java)
                }
                startActivity(i)
                finish()
            } else {
                Util.showMessage(EducationApp.appContext, "No internet connection")
            }
        }, 2000.toLong())
    }

    // materialDialog.dismiss();
    private fun getServerConfig() {
        val call = APIClient(HOSTURL).getApi.loadServerConfig("36")
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.body() != null) {
                    try {
                        val data = response.body()!!.string()
                        AppLogger.logger.D("Keyur", data)
                        AppPrefConfig.pref.serverProfile = data
                        EducationApp.configModel =
                            Gson().fromJson(data, ConfigServerModel::class.java)

                        if (AppPrefConfig.pref.userID.isNotEmpty()) {
                            validateToken()
                        } else {
                            initView(1000)
                        }

                    } catch (e: Exception) {
                        initView(1000)
                        AppLogger.logger.D("Keyur", "failed data ${e.message}")
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                // materialDialog.dismiss();
                initView(1000)
                AppLogger.logger.D("Keyur", "failed data ${t.message}")
            }
        })
    }

    override fun onResume() {
        super.onResume()

    }
}