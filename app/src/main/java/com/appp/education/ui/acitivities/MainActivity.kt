package com.appp.education.ui.acitivities

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.webkit.CookieManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.appp.education.EducationApp
import com.appp.education.R
import com.appp.education.data.appPrefrence.AppPrefConfig
import com.appp.education.data.networkData.APIClient
import com.appp.education.databinding.ActivityMainBinding
import com.appp.education.model.DashBoardModel
import com.appp.education.ui.DashBoardAdapter
import com.appp.education.utils.Constant
import com.appp.education.utils.Constant.LOCK_REQUEST_CODE
import com.appp.education.utils.Constant.SECURITY_SETTING_REQUEST_CODE
import com.appp.education.utils.Util
import com.appp.education.utils.Util.decode
import com.appp.education.utils.Util.encode
import com.bumptech.glide.Glide
import com.infoblox.bloxone.util.AppLogger
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.navView.mainHeader.logout.typeface = EducationApp.mediumFont
        binding.navView.mainHeader.changePassword.typeface = EducationApp.mediumFont
        binding.navView.mainHeader.autheticationView.typeface = EducationApp.mediumFont
        binding.navView.mainHeader.userNameTV.typeface = EducationApp.mediumFont

        loadUrlInWebView("${EducationApp.configModel.home_path}/MobileApp/dashboardBanner")

        val userData = AppPrefConfig.pref.userProfile


        if (userData.isEmpty()) {
            loadUserProfile()
        } else {
            loadUserData(userData)
        }

        menuItem.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
        }
        binding.navView.mainHeader.logout.setOnClickListener {

            AppPrefConfig.pref.clearData()
            startActivity(Intent(it.context, LoginActivity::class.java))
            finish()
        }

        binding.navView.mainHeader.changePassword.setOnClickListener {

            startActivity(
                Intent(this@MainActivity, WebViewActivity::class.java)
                    .putExtra(
                        Constant.CLIENTDATA,
                        "${EducationApp.configModel.home_path}/change-password"
                    )
                    .putExtra(Constant.AUTHTOKEN, Constant.EMPTY)
                    .putExtra(Constant.NAME, "Change Password")
            )
        }
        binding.navView.mainHeader.authFingurePrintSwitch.apply {
            isChecked = AppPrefConfig.pref.isAuthEnabled
            binding.navView.mainHeader.authFingurePrintSwitch.setOnCheckedChangeListener { _, isChecked ->
                AppPrefConfig.pref.isAuthEnabled = isChecked
            }

            if (isChecked) {
                authenticateApp()
            }
        }

        val dataItem = ArrayList<DashBoardModel>().apply {


            val user = AppPrefConfig.pref.userID.split("|")

            val gridData = when (user[0]) {
                "h" -> {
                    EducationApp.configModel.tiles_h
                }
                "t" -> {
                    EducationApp.configModel.tiles_t
                }
                "s" -> {
                    EducationApp.configModel.tiles_s
                }
                else -> {
                    EducationApp.configModel.tiles_c
                }
            }

            AppLogger.logger.D(Constant.TAG, "gridData -- $gridData")

            val jsonArray = JSONArray(gridData)


            val drawbleData = "drawable"
            val pacakge = packageName;

            for (i in 0 until jsonArray.length()) {
                val item = jsonArray.getJSONObject(i)

                val icon = item.getString("icon").split("/")
                val subIcon = icon[1].split(".")
                add(
                    DashBoardModel(
                        item.getString("label"),
                        Util.getResourceId(subIcon[0].replace("-", "_"), drawbleData, pacakge),
                        item.getString("route")
                    )
                )
            }

            val dashBoardAdapter = DashBoardAdapter(this, object : DashBoardAdapter.ItemListener {
                override fun onItemClick(item: DashBoardModel?) {


                    if (item == null) {
                        AppLogger.logger.D(Constant.TAG, " null;;;;;;")
                    }

                    val user = AppPrefConfig.pref.userID.split("|")

                    val authToken: String = "${user[1]}:${user[3]}".encode().replace("\n", "")

                    val urlFinal = "${EducationApp.configModel.home_path}${item?.route}?auth_token=$authToken"
                    AppLogger.logger.D(Constant.TAG, " authtoken -- $authToken")
                    AppLogger.logger.D(Constant.TAG, " authtoken -- $urlFinal")

                    startActivity(
                        Intent(this@MainActivity, WebViewActivity::class.java)
                            .putExtra(Constant.CLIENTDATA, urlFinal)
                            .putExtra(Constant.AUTHTOKEN, authToken)
                            .putExtra(Constant.NAME, item!!.title)
                    )
                }
            })
            binding.appBarMain.contentview.homeRecyclerView.apply {

                val layoutManager =
                    GridLayoutManager(context, 3);//AutoFitGridLayoutManager(context, 320)
                setLayoutManager(layoutManager)
                adapter = dashBoardAdapter

            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUrlInWebView(url: String) {


        val myWebView = binding.appBarMain.contentview.sliderView

        // val authToken = Base64.getEncoder().encode()
        AppLogger.logger.D(Constant.TAG, " web dashboard -- $url")

        val user = AppPrefConfig.pref.userID.split("|")


        if (user[0] != EducationApp.configModel.show_app_banner_for) {

            myWebView.visibility = View.GONE

            return
        }

        val authToken: String = "${user[1]}:${user[3]}".encode()

        val urlFinal = "$url?auth_token=$authToken"
        AppLogger.logger.D(Constant.TAG, " authtoken -- $authToken")
        AppLogger.logger.D(Constant.TAG, " urlFinal -- $urlFinal")


        myWebView.settings.javaScriptEnabled = true
        myWebView.settings.userAgentString = "AndroidApp"

        val cookie = CookieManager.getInstance().getCookie(url)

        if (cookie != null) {
            if (!cookie.contains(authToken)) {
                CookieManager.getInstance().setCookie(url, "auth_hash=$authToken;")
                CookieManager.getInstance()
                    .setCookie(url, "selected_school=${EducationApp.configModel.school_id};")
            }
        } else {
            CookieManager.getInstance().setCookie(url, "auth_hash=$authToken;")
            CookieManager.getInstance()
                .setCookie(url, "selected_school=${EducationApp.configModel.school_id};")
        }

        val cookie2 = CookieManager.getInstance().getCookie(url)

        AppLogger.logger.D(Constant.TAG, "cookieManager $cookie2")
        CookieManager.getInstance().setAcceptThirdPartyCookies(myWebView, true);

        myWebView.settings.javaScriptEnabled = true
        myWebView.loadUrl(urlFinal)

        AppLogger.logger.D(Constant.TAG, " authtoken -- ${authToken.decode()}")

    }


    private fun loadUserData(data: String) {
        val userProfile = data.split("|")

        if (userProfile[1].startsWith("/res")) {
            binding.navView.mainHeader.userImageView.setImageResource(R.drawable.default_user)
        } else {
            Glide.with(this).load(userProfile[1]).error(R.drawable.default_user)
                .into(binding.navView.mainHeader.userImageView)
        }
        userNameTV.text = userProfile[0]

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            LOCK_REQUEST_CODE -> {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    // If screen lock authentication is success update text
                } else {
                    authenticateApp()
                }
            }
            SECURITY_SETTING_REQUEST_CODE -> {
                // When user is enabled Security settings then we don't get any kind of RESULT_OK
                // So we need to check whether device has enabled screen lock or not
                if (!isDeviceSecure()) {
                    authenticateApp()
                }
            }

        }
    }

    private fun isDeviceSecure(): Boolean {
        val keyguardManager = getSystemService(KEYGUARD_SERVICE) as KeyguardManager?

        // this method only work whose api level is greater than or equal to Jelly_Bean (16)
        return keyguardManager!!.isDeviceSecure

        // You can also use keyguardManager.isDeviceSecure(); but it requires API Level 23
    }

    private fun loadUserProfile() {

        val dataArray = AppPrefConfig.pref.userID.split("|")

        val call =
            APIClient("${EducationApp.configModel.api_path}/").getApi.fetUser(

                dataArray[1],
                dataArray[3]
            )
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.body() != null) {
                    try {
                        val data = response.body()!!.string()
                        AppLogger.logger.D("Keyur", "login data $data")

                        AppPrefConfig.pref.userProfile = data

                        loadUserData(data)


                    } catch (e: Exception) {
                        AppLogger.logger.D("Keyur", "failed data ${e.message}")

                    }
                } else {
                    val eBody = response.errorBody()
                    eBody?.let { Util.showMessage(applicationContext, it.string()) }

                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                // materialDialog.dismiss();
                Util.showMessage(applicationContext, t.message)
                AppLogger.logger.D("Keyur", "failed data ${t.message}")

            }
        })
    }


    // method to authenticate app
    private fun authenticateApp() {
        // Get the instance of KeyGuardManager
        val keyguardManager = getSystemService(KEYGUARD_SERVICE) as KeyguardManager?

        // Check if the device version is greater than or equal to Lollipop(21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Create an intent to open device screen lock screen to authenticate
            // Pass the Screen Lock screen Title and Description
            val i = keyguardManager!!.createConfirmDeviceCredentialIntent(
                getString(R.string.unLockTitle),
                getString(R.string.unlock)
            )
            try {
                // Start activity for result
                startActivityForResult(i, LOCK_REQUEST_CODE)
            } catch (e: Exception) {

                // If some exception occurs means Screen lock is not set up please set screen lock
                // Open Security screen directly to enable patter lock
                val intent = Intent(Settings.ACTION_SECURITY_SETTINGS)
                try {

                    // Start activity for result
                    startActivityForResult(intent, SECURITY_SETTING_REQUEST_CODE)
                } catch (ex: Exception) {

                }
            }
        }
    }
}