package com.appp.education.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.appp.education.EducationApp
import com.appp.education.R
import com.appp.education.model.DashBoardModel
import java.util.*

open class DashBoardAdapter(
    var mValues: ArrayList<DashBoardModel>,
    protected var mListener: ItemListener
) : RecyclerView.Adapter<DashBoardAdapter.ViewHolder>() {
    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var textView: AppCompatTextView
        var imageView: AppCompatImageView
        var item: DashBoardModel? = null
        fun setData(item: DashBoardModel) {
            this.item = item
            textView.text = item.title
            textView.typeface = EducationApp.mediumFont
            imageView.setImageResource(item.drawable)
        }

        override fun onClick(view: View) {
            if (mListener != null) {
                mListener!!.onItemClick(item)
            }
        }

        init {
            v.setOnClickListener(this)
            textView = v.findViewById<View>(R.id.textView) as AppCompatTextView
            imageView = v.findViewById<View>(R.id.imageView) as AppCompatImageView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(Vholder: ViewHolder, position: Int) {
        Vholder.setData(mValues[position])
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    interface ItemListener {
        fun onItemClick(item: DashBoardModel?)
    }
}