package com.appp.education

import android.app.Application
import android.content.Context
import android.graphics.Typeface
import android.net.ConnectivityManager
import com.appp.education.model.ConfigServerModel

/**
 * Created by THKEYUR on 31/07/2021.
 */
class EducationApp : Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        lightFont =
            Typeface.createFromAsset(assets, "SF-Pro-Display-Light.otf")
        boldFont =
            Typeface.createFromAsset(assets, "SF-Pro-Display-Bold.otf")
        mediumFont =
            Typeface.createFromAsset(assets, "SF-Pro-Display-Medium.otf")
        regularFont =
            Typeface.createFromAsset(assets, "SF-Pro-Display-Regular.otf")
        semiBoldFont =
            Typeface.createFromAsset(assets, "SF-Pro-Display-Semibold.otf")
    }


    companion object {
        lateinit var appContext: Context
        lateinit var configModel: ConfigServerModel
        lateinit var lightFont: Typeface
        lateinit var boldFont: Typeface
        lateinit var mediumFont: Typeface
        lateinit var semiBoldFont: Typeface
        lateinit var regularFont: Typeface

        val isConnected: Boolean
            get() {
                val cm = appContext!!.applicationContext
                    .getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = cm.activeNetworkInfo
                return (activeNetwork != null
                        && activeNetwork.isConnectedOrConnecting)
            }
    }
}